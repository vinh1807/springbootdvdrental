# DVDRental-SpringBoot

This project for learning java spring with hibernate jpa.

## Demo Screenshot

![Scheme](images/SpringProjectStrut.jpg)
![Scheme](images/SpringBootRunScreen.jpg)
![Scheme](images/SpringRestAPI.jpg)

## Tools and Enviroments

JDK: 1.8.  
IDE: Spring Tool Suite 3.9.4.RELEASE. (STS)

### Starting project

Right-click on root project directory in STS -> Run as -> Spring Boot App.

## Running the tests

Right-click on root project directory (or package, test class) in STS -> Run as -> JUnit Test.
