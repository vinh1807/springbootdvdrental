package dvdrental.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.dvdrental.controller.ActorController;
import com.dvdrental.entity.Actor;
import com.dvdrental.service.ActorService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class ActorControllerTest {

	String jsonArray;
	String jsonObject;

	Actor actor = new Actor("vinh", "le tran");

	private MockMvc mvc;

	@InjectMocks
	ActorController actorController;

	@Mock
	private ActorService actorService;

	@Before
	public void setUp() throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();

		actor.setActorId(1);
		jsonObject = mapper.writeValueAsString(actor);
		List<Actor> actors = Arrays.asList(actor);
		jsonArray = mapper.writeValueAsString(actors);

		mvc = MockMvcBuilders.standaloneSetup(actorController).build();
		when(actorService.getAllActors()).thenReturn(actors);
		when(actorService.getAllActorsByPage("actorId", 1, 5)).thenReturn(actors);
		when(actorService.getActorById(1)).thenReturn(actor);
	}

	@Test
	public void givenActors_whenGetAllActors_thenReturnJsonArray() throws Exception {
		mvc.perform(get("/actor/all").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonArray));

		verify(actorService, times(1)).getAllActors();
	}

	@Test
	public void givenActors_whenGetAllActorsByPage_thenReturnJsonArray() throws Exception {
		mvc.perform(get("/actor/").accept(MediaType.APPLICATION_JSON).param("page", "1").param("orderBy", "actorId")
				.param("pageSize", "5")).andExpect(status().isOk()).andExpect(content().json(jsonArray));

		verify(actorService, times(1)).getAllActorsByPage("actorId", 1, 5);
	}

	@Test
	public void givenActor_whenGetActorById_thenReturnJsonObject() throws Exception {
		mvc.perform(get("/actor/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(actorService, times(1)).getActorById(1);
	}

	@Test
	public void whenSaveActor_thenReturnSuccess() throws Exception {
		mvc.perform(post("/actor/").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(actorService, times(1)).saveActor(any(Actor.class));
	}

	@Test
	public void whenUpdateActor_thenReturnSuccess() throws Exception {
		mvc.perform(put("/actor/").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(actorService, times(1)).updateActor(any(Actor.class));
	}

	@Test
	public void whenDeleteActor_thenReturnSuccess() throws Exception {
		mvc.perform(delete("/actor/1").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

		verify(actorService, times(1)).deleteActor(1);
	}

}
