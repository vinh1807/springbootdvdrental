package dvdrental.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.dvdrental.controller.RentalController;
import com.dvdrental.entity.Customer;
import com.dvdrental.entity.Inventory;
import com.dvdrental.entity.Rental;
import com.dvdrental.entity.Staff;
import com.dvdrental.service.RentalService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class RentalControllerTest {

	String jsonArray;
	String jsonObject;

	Rental rental = new Rental(new Customer(), new Inventory(), new Staff(), new Date(System.currentTimeMillis()),
			new Date(System.currentTimeMillis()));

	private MockMvc mvc;

	@InjectMocks
	RentalController rentalController;

	@Mock
	private RentalService rentalService;

	@Before
	public void setUp() throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();

		rental.setRentalId(1);
		jsonObject = mapper.writeValueAsString(rental);
		List<Rental> rentals = Arrays.asList(rental);
		jsonArray = mapper.writeValueAsString(rentals);

		mvc = MockMvcBuilders.standaloneSetup(rentalController).build();
		when(rentalService.getAllRentals()).thenReturn(rentals);
		when(rentalService.getAllRentalsByPage("rentalId", 1, 5)).thenReturn(rentals);
		when(rentalService.getRentalById(1)).thenReturn(rental);
	}

	@Test
	public void givenRentals_whenGetAllRentals_thenReturnJsonArray() throws Exception {
		mvc.perform(get("/rental/all").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonArray));

		verify(rentalService, times(1)).getAllRentals();
	}

	@Test
	public void givenRentals_whenGetAllRentalsByPage_thenReturnJsonArray() throws Exception {
		mvc.perform(get("/rental/").accept(MediaType.APPLICATION_JSON).param("page", "1").param("orderBy", "rentalId")
				.param("pageSize", "5")).andExpect(status().isOk()).andExpect(content().json(jsonArray));

		verify(rentalService, times(1)).getAllRentalsByPage("rentalId", 1, 5);
	}

	@Test
	public void givenRental_whenGetRentalById_thenReturnJsonObject() throws Exception {
		mvc.perform(get("/rental/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(rentalService, times(1)).getRentalById(1);
	}

	@Test
	public void whenSaveRental_thenReturnSuccess() throws Exception {
		mvc.perform(post("/rental/").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(rentalService, times(1)).saveRental(any());
	}

	@Test
	public void whenUpdateRental_thenReturnSuccess() throws Exception {
		mvc.perform(put("/rental/").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(rentalService, times(1)).updateRental(any());
	}

	@Test
	public void whenDeleteRental_thenReturnSuccess() throws Exception {
		mvc.perform(delete("/rental/1").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
				.andExpect(content().string("1"));

		verify(rentalService, times(1)).deleteRental(1);
	}

}
