package dvdrental.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.dvdrental.controller.CustomerController;
import com.dvdrental.entity.Address;
import com.dvdrental.entity.Customer;
import com.dvdrental.service.CustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {

	String jsonArray;
	String jsonObject;

	Customer customer = new Customer(new Address(), (short) 1, "vinh", "le tran", new Date(System.currentTimeMillis()));

	private MockMvc mvc;

	@InjectMocks
	CustomerController customerController;

	@Mock
	private CustomerService customerService;

	@Before
	public void setUp() throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();

		customer.setCustomerId(1);
		jsonObject = mapper.writeValueAsString(customer);
		List<Customer> customers = Arrays.asList(customer);
		jsonArray = mapper.writeValueAsString(customers);

		mvc = MockMvcBuilders.standaloneSetup(customerController).build();
		when(customerService.getAllCustomers()).thenReturn(customers);
		when(customerService.getAllCustomersByPage("customerId", 1, 5)).thenReturn(customers);
		when(customerService.getCustomerById(1)).thenReturn(customer);
	}

	@Test
	public void givenCustomers_whenGetAllCustomers_thenReturnJsonArray() throws Exception {
		mvc.perform(get("/customer/all").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonArray));

		verify(customerService, times(1)).getAllCustomers();
	}

	@Test
	public void givenCustomers_whenGetAllCustomersByPage_thenReturnJsonArray() throws Exception {
		mvc.perform(get("/customer/").accept(MediaType.APPLICATION_JSON).param("page", "1")
				.param("orderBy", "customerId").param("pageSize", "5")).andExpect(status().isOk())
				.andExpect(content().json(jsonArray));

		verify(customerService, times(1)).getAllCustomersByPage("customerId", 1, 5);
	}

	@Test
	public void givenCustomer_whenGetCustomerById_thenReturnJsonObject() throws Exception {
		mvc.perform(get("/customer/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(customerService, times(1)).getCustomerById(1);
	}

	@Test
	public void whenSaveCustomer_thenReturnSuccess() throws Exception {
		mvc.perform(post("/customer/").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(customerService, times(1)).saveCustomer(any());
	}

	@Test
	public void whenUpdateCustomer_thenReturnSuccess() throws Exception {
		mvc.perform(put("/customer/").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(customerService, times(1)).updateCustomer(any());
	}

	@Test
	public void whenDeleteCustomer_thenReturnSuccess() throws Exception {
		mvc.perform(delete("/customer/1").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string("1"));

		verify(customerService, times(1)).deleteCustomer(1);
	}

}
