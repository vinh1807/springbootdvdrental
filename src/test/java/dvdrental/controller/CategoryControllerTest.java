package dvdrental.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.dvdrental.controller.CategoryController;
import com.dvdrental.entity.Category;
import com.dvdrental.service.CategoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class CategoryControllerTest {

	String jsonArray;
	String jsonObject;

	Category category = new Category("thriller");

	private MockMvc mvc;

	@InjectMocks
	CategoryController categoryController;

	@Mock
	private CategoryService categoryService;

	@Before
	public void setUp() throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();

		category.setCategoryId(1);
		jsonObject = mapper.writeValueAsString(category);
		List<Category> categories = Arrays.asList(category);
		jsonArray = mapper.writeValueAsString(categories);

		mvc = MockMvcBuilders.standaloneSetup(categoryController).build();
		when(categoryService.getAllCategories()).thenReturn(categories);
		// when(categoryService.getAllCategoriesByPage("categoryId", 1,
		// 5)).thenReturn(categories);
		when(categoryService.getCategoryById(1)).thenReturn(category);
	}

	@Test
	public void givenCategories_whenGetAllCategories_thenReturnJsonArray() throws Exception {
		mvc.perform(get("/category/all").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonArray));

		verify(categoryService, times(1)).getAllCategories();
	}

	// @Test
	// public void givenCategories_whenGetAllCategoriesByPage_thenReturnJsonArray()
	// throws Exception {
	// mvc.perform(get("/category/").accept(MediaType.APPLICATION_JSON).param("page",
	// "1")
	// .param("orderBy", "categoryId").param("pageSize",
	// "5")).andExpect(status().isOk())
	// .andExpect(content().json(jsonArray));
	//
	// verify(categoryService, times(1)).getAllCategoriesByPage("categoryId", 1, 5);
	// }

	@Test
	public void givenCategory_whenGetCategoryById_thenReturnJsonObject() throws Exception {
		mvc.perform(get("/category/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(categoryService, times(1)).getCategoryById(1);
	}

	@Test
	public void whenSaveCategory_thenReturnSuccess() throws Exception {
		mvc.perform(post("/category/").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(categoryService, times(1)).saveCategory(any(Category.class));
	}

	@Test
	public void whenUpdateCategory_thenReturnSuccess() throws Exception {
		mvc.perform(put("/category/").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(categoryService, times(1)).updateCategory(any(Category.class));
	}

	@Test
	public void whenDeleteCategory_thenReturnSuccess() throws Exception {
		mvc.perform(delete("/category/1").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string("1"));

		verify(categoryService, times(1)).deleteCategory(1);
	}

}
