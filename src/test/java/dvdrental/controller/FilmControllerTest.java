package dvdrental.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.dvdrental.controller.FilmController;
import com.dvdrental.entity.Film;
import com.dvdrental.entity.Language;
import com.dvdrental.service.FilmService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class FilmControllerTest {

	String jsonArray;
	String jsonObject;
	Film film = new Film(new Language("DSAD"), "lolo", (short) 4, 5, 5, 2000);

	private MockMvc mvc;

	@InjectMocks
	FilmController filmController;

	@Mock
	private FilmService filmService;

	@Before
	public void setUp() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();

		film.setFilmId(1);
		jsonObject = mapper.writeValueAsString(film);
		List<Film> films = Arrays.asList(film);
		jsonArray = mapper.writeValueAsString(films);

		mvc = MockMvcBuilders.standaloneSetup(filmController).build();
		when(filmService.getAllFilms()).thenReturn(films);
		when(filmService.getAllFilmsByPage("filmId", 1, 5)).thenReturn(films);
		when(filmService.getFilmById(1)).thenReturn(film);
	}

	@Test
	public void givenFilms_whenGetAllFilms_thenReturnJsonArray() throws Exception {
		mvc.perform(get("/film/all").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonArray));

		verify(filmService, times(1)).getAllFilms();
	}

	@Test
	public void givenFilms_whenGetAllFilmsByPage_thenReturnJsonArray() throws Exception {
		mvc.perform(get("/film/").accept(MediaType.APPLICATION_JSON).param("page", "1").param("orderBy", "filmId").param("pageSize", "5"))
				.andExpect(status().isOk()).andExpect(content().json(jsonArray));

		verify(filmService, times(1)).getAllFilmsByPage("filmId", 1, 5);
	}

	@Test
	public void givenFilm_whenGetFilmById_thenReturnJsonObject() throws Exception {
		mvc.perform(get("/film/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(filmService, times(1)).getFilmById(1);
	}

	@Test
	public void whenSaveFilm_thenReturnSuccess() throws Exception {
		mvc.perform(post("/film/").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(filmService, times(1)).saveFilm(any());
	}

	@Test
	public void whenUpdateFilm_thenReturnSuccess() throws Exception {
		mvc.perform(put("/film/").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject)).andExpect(status().isOk())
				.andExpect(content().json(jsonObject));

		verify(filmService, times(1)).updateFilm(any());
	}

	@Test
	public void whenDeleteFilm_thenReturnSuccess() throws Exception {
		mvc.perform(delete("/film/1").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
				).andExpect(status().isOk())
				.andExpect(content().string("1"));

		verify(filmService, times(1)).deleteFilm(1);
	}
}
