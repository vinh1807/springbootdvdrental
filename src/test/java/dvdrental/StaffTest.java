package dvdrental;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dvdrental.Springdvdrental2Application;
import com.dvdrental.entity.Staff;
import com.dvdrental.service.AddressService;
import com.dvdrental.service.StaffService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=Springdvdrental2Application.class)
public class StaffTest {
	
	@Autowired
	private StaffService staffService;
	
	@Autowired
	private AddressService addressService;
	
	@Test
	public void testGetStaffById() {
		Staff staff = new Staff(addressService.getAddressById(1), "vinh", "le tran", (short)1, true, "vinh11", "123456");
		staffService.saveStaff(staff);
		assertNotEquals(0, staff.getStaffId());

		Staff fromDB = staffService.getStaffById(staff.getStaffId());
		assertEquals("vinh", fromDB.getFirstName());
	}

	@Test
	public void testUpdateStaff() {
		Staff staff = new Staff(addressService.getAddressById(1), "vinh", "le tran", (short)1, true, "vinh11", "123456");
		staffService.saveStaff(staff);
		
		staff.setPassword("newpass");
		staffService.updateStaff(staff);
		
		Staff fromDB = staffService.getStaffById(staff.getStaffId());
		assertEquals("newpass", fromDB.getPassword());
	}
	
	@Test
	public void testDeleteStaff() {
		Staff staff = new Staff(addressService.getAddressById(1), "vinh", "le tran", (short)1, true, "vinh11", "123456");
		staffService.saveStaff(staff);
		
		staffService.deleteStaff(staff.getStaffId());
	
		Staff fromDB = staffService.getStaffById(staff.getStaffId());
		assertEquals(null, fromDB);
	}
	
	@Test
	public void testGetAllStaffs() {
		Staff staff1 = new Staff(addressService.getAddressById(1), "vinh", "le tran", (short)1, true, "vinh11", "123456");
		Staff staff2 = new Staff(addressService.getAddressById(1), "vinh", "le tran", (short)1, true, "vinh22", "123456");
		Staff staff3 = new Staff(addressService.getAddressById(1), "vinh", "le tran", (short)1, true, "vinh33", "123456");
		staffService.saveStaff(staff1);
		staffService.saveStaff(staff2);
		staffService.saveStaff(staff3);

		List<Staff> staffsFromDB = staffService.getAllStaffs();
		assertTrue(staffsFromDB.size() >= 3);
	}

	@Test
	public void whenInvalidId_thenReturnNull() {
		Staff fromDB = staffService.getStaffById(435345);
		assertEquals(null, fromDB);
	}
	
}
