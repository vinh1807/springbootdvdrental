package dvdrental;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dvdrental.Springdvdrental2Application;
import com.dvdrental.entity.Address;
import com.dvdrental.service.AddressService;
import com.dvdrental.service.CityService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=Springdvdrental2Application.class)
public class AddressTest {

	@Autowired
	private AddressService addressService;
	
	@Autowired
	private CityService cityService;

	@Test
	public void testGetAddress() {
		Address address = new Address("asdsad", "asdasd", "asdsad", "asdads",
					"asdasd", new Date(System.currentTimeMillis()), cityService.getCityById(1));
		addressService.saveAddress(address);
		 
		Address addresssFromDB = addressService.getAddressById(address.getAddressId());

		assertEquals(addresssFromDB.getAddress(), address.getAddress());
	}
	
	@Test
	public void testSaveAddress() {
		Address address = new Address("asdssfad", "asdasasd", "awwsdsad", "aseedads",
				"asdasd", new Date(System.currentTimeMillis()), cityService.getCityById(1));

		assertEquals(0, address.getAddressId());
		addressService.saveAddress(address);
		assertNotEquals(0, address.getAddressId());
		Address fromDB = addressService.getAddressById(address.getAddressId());
		assertEquals(address.getAddress(), fromDB.getAddress());
		assertEquals(address.getAddress2(), fromDB.getAddress2());
	}
	
	@Test
	public void testUpdateAddress() {
		Address address = new Address("asdsad", "asdasd", "asdsad", "asdads",
				"asdasd", new Date(System.currentTimeMillis()), cityService.getCityById(1));
		addressService.saveAddress(address);
		
		address.setAddress("hochiminh");
		addressService.updateAddress(address);
		
		Address fromDB = addressService.getAddressById(address.getAddressId());
		assertEquals("hochiminh", fromDB.getAddress());
	}
	
	@Test
	public void whenInvalidId_thenReturnNull() {
		Address addresssFromDB = addressService.getAddressById(1234);
		assertEquals(null, addresssFromDB);
	}
	
	@Test
	public void testGetAllActor() {
		Address address1 = new Address("asdsad1", "asdasd1", "asdsad1", "asdads1",
				"asdasd1", new Date(System.currentTimeMillis()), cityService.getCityById(1));
		Address address2 = new Address("asdsad2", "asdasd2", "asdsad2", "asdads2",
				"asdasd2", new Date(System.currentTimeMillis()), cityService.getCityById(1));
		Address address3 = new Address("asdsad3", "asdasd3", "asdsad3", "asdads3",
				"asdasd3", new Date(System.currentTimeMillis()), cityService.getCityById(1));
		
		addressService.saveAddress(address1);
		addressService.saveAddress(address2);
		addressService.saveAddress(address3);

		List<Address> addressesFromDB = addressService.getAllAddresses();
		assertTrue(addressesFromDB.size() >= 3);
	}

}
