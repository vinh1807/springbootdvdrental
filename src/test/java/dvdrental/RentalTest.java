package dvdrental;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dvdrental.Springdvdrental2Application;
import com.dvdrental.entity.Rental;
import com.dvdrental.service.CustomerService;
import com.dvdrental.service.InventoryService;
import com.dvdrental.service.RentalService;
import com.dvdrental.service.StaffService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=Springdvdrental2Application.class)
public class RentalTest {

	@Autowired
	private RentalService rentalService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private InventoryService inventoryService;
	
	@Autowired
	private StaffService staffService;

	@Test
	public void testGetRentalById() {
		Rental rental = new Rental(customerService.getCustomerById(1), inventoryService.getInventoryById(1),
				staffService.getStaffById(1), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
		rentalService.saveRental(rental);
		
		Rental fromDB = rentalService.getRentalById(rental.getRentalId());
		assertNotNull(fromDB);
	}

	@Test
	public void testUpdateRental() {
		Rental rental = new Rental(customerService.getCustomerById(1), inventoryService.getInventoryById(1),
				staffService.getStaffById(1), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
		rentalService.saveRental(rental);
		try {
			TimeUnit.MILLISECONDS.sleep(10);;;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Date date = new Date(System.currentTimeMillis());
		assertNotEquals(date, rental.getReturnDate());
		rental.setRentalDate(date);
		rentalService.updateRental(rental);
		
		Rental fromDB = rentalService.getRentalById(rental.getRentalId());
		assertEquals(rental.getReturnDate(), fromDB.getReturnDate());
	}
	
	@Test
	public void testDeleteRental() {
		Rental rental = new Rental(customerService.getCustomerById(1), inventoryService.getInventoryById(1),
				staffService.getStaffById(1), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
		rentalService.saveRental(rental);
		
		assertNotEquals(0, rental.getRentalId());
		rentalService.deleteRental(rental.getRentalId());
	
		Rental fromDB = rentalService.getRentalById(rental.getRentalId());
		assertEquals(null, fromDB);
	}

	@Test
	public void testGetAllRental() {
		Rental rental1 = new Rental(customerService.getCustomerById(1), inventoryService.getInventoryById(1),
				staffService.getStaffById(1), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
		Rental rental2 = new Rental(customerService.getCustomerById(2), inventoryService.getInventoryById(2),
				staffService.getStaffById(1), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
		Rental rental3 = new Rental(customerService.getCustomerById(3), inventoryService.getInventoryById(3),
				staffService.getStaffById(1), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
		
		rentalService.saveRental(rental1);
		rentalService.saveRental(rental2);
		rentalService.saveRental(rental3);

		List<Rental> rentalsFromDB = rentalService.getAllRentals();
		assertTrue(rentalsFromDB.size() >= 3);
	}


	@Test
	public void whenInvalidId_thenReturnNull() {
		Rental fromDB = rentalService.getRentalById(435345);
		assertEquals(null, fromDB);
	}

}
