package dvdrental;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dvdrental.Springdvdrental2Application;
import com.dvdrental.entity.Customer;
import com.dvdrental.service.AddressService;
import com.dvdrental.service.CustomerService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=Springdvdrental2Application.class)
public class CustomerTest {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private AddressService addressService;
	
	@Test
	public void testGetCustomerById() {
		Customer customer = new Customer(addressService.getAddressById(1), (short)1, "vinh", "le tran"
				, new Date(System.currentTimeMillis()));
		customerService.saveCustomer(customer);
		assertNotEquals(0, customer.getCustomerId());

		Customer fromDB = customerService.getCustomerById(customer.getCustomerId());
		assertEquals("vinh", fromDB.getFirstName());
	}
	
	@Test
	public void testGetCustomersByName() {
		Customer customer = new Customer(addressService.getAddressById(1), (short)1, "vinh", "le tran"
				, new Date(System.currentTimeMillis()));
		customerService.saveCustomer(customer);
		Customer customer1 = new Customer(addressService.getAddressById(1), (short)1, "hniv", "le tran"
				, new Date(System.currentTimeMillis()));
		customerService.saveCustomer(customer1);

		List<Customer> fromDBs = customerService.getCustomersByName("vinh");
		for (Customer fromDB : fromDBs) {
			assertTrue(fromDB.getFirstName().toLowerCase().contains("vinh"));
		}
	}
	
	@Test
	public void testUpdateCustomer() {
		Customer customer = new Customer(addressService.getAddressById(1), (short)1, "vinh", "le tran"
				, new Date(System.currentTimeMillis()));
		customerService.saveCustomer(customer);
		
		customer.setFirstName("newname");
		customerService.updateCustomer(customer);
		
		Customer fromDB = customerService.getCustomerById(customer.getCustomerId());
		assertEquals("newname", fromDB.getFirstName());
	}
	
	@Test
	public void testDeleteCustomer() {
		Customer customer = new Customer(addressService.getAddressById(1), (short)1, "vinh", "le tran"
				, new Date(System.currentTimeMillis()));
		customerService.saveCustomer(customer);
		
		customerService.deleteCustomer(customer.getCustomerId());
	
		Customer fromDB = customerService.getCustomerById(customer.getCustomerId());
		assertEquals(null, fromDB);
	}
	
	@Test
	public void testGetAllCustomers() {
		Customer customer = new Customer(addressService.getAddressById(1), (short)1, "vinh", "le tran"
				, new Date(System.currentTimeMillis()));
		Customer customer1 = new Customer(addressService.getAddressById(1), (short)1, "vinh1", "le tran"
				, new Date(System.currentTimeMillis()));
		Customer customer2 = new Customer(addressService.getAddressById(1), (short)1, "vinh2", "le tran"
				, new Date(System.currentTimeMillis()));
		customerService.saveCustomer(customer);
		customerService.saveCustomer(customer1);
		customerService.saveCustomer(customer2);

		List<Customer> customersFromDB = customerService.getAllCustomers();
		assertTrue(customersFromDB.size() >= 3);
	}
	
	@Test
	public void whenInvalidName_thenReturnZeroList() {
		List<Customer> fromDBs = customerService.getCustomersByName("invalidname");
		assertEquals(0, fromDBs.size());
	}
	
	@Test
	public void whenInvalidId_thenReturnNull() {
		Customer fromDB = customerService.getCustomerById(435345);
		assertEquals(null, fromDB);
	}
	
}
