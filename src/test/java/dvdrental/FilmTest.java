package dvdrental;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dvdrental.Springdvdrental2Application;
import com.dvdrental.entity.Film;
import com.dvdrental.service.FilmService;
import com.dvdrental.service.LanguageService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=Springdvdrental2Application.class)
public class FilmTest {
	
	@Autowired
	private FilmService filmService;
	
	@Autowired
	private LanguageService languageService;
	
	@Test
	public void testGetFilmById() {
		Film film = new Film(languageService.getLanguageById(1), "lolo", (short)4, 5
				, 5, 2000);
		filmService.saveFilm(film);
		assertNotEquals(0, film.getFilmId());
	
		Film fromDB = filmService.getFilmById(film.getFilmId());
		assertEquals("lolo", fromDB.getTitle());
	}
	
	@Test
	public void testGetFilmByTitle() {
		Film film = new Film(languageService.getLanguageById(1), "lolo1", (short)4, 5
				, 5, 2000);
		filmService.saveFilm(film);
		Film film1 = new Film(languageService.getLanguageById(1), "asas2", (short)4, 5
				, 5, 2000);
		filmService.saveFilm(film1);
		
		List<Film> fromDBs = filmService.getFilmsByTitle("lolo");
		for (Film fromDB : fromDBs) {
			assertTrue(fromDB.getTitle().toLowerCase().contains("lolo"));
		}
	}
	
	@Test
	public void testUpdateFilm() {
		Film film = new Film(languageService.getLanguageById(1), "lolo", (short)4, 5
				, 5, 2000);
		filmService.saveFilm(film);
		
		film.setTitle("newtitle");
		filmService.updateFilm(film);
		
		Film fromDB = filmService.getFilmById(film.getFilmId());
		assertEquals("newtitle", fromDB.getTitle());
	}
	
	@Test
	public void testDeleteFilm() {
		Film film = new Film(languageService.getLanguageById(1), "lolo", (short)4, 5
				, 5, 2000);
		filmService.saveFilm(film);
		assertNotEquals(0, film.getFilmId());
		filmService.deleteFilm(film.getFilmId());
	
		Film fromDB = filmService.getFilmById(film.getFilmId());
		assertEquals(null, fromDB);
	}
	
	@Test
	public void testGetAllActor() {
		Film film = new Film(languageService.getLanguageById(1), "lolo1", (short)4, 5
				, 5, 2000);
		filmService.saveFilm(film);
		Film film1 = new Film(languageService.getLanguageById(1), "asas2", (short)4, 5
				, 5, 2000);
		filmService.saveFilm(film1);

		List<Film> filmsFromDB = filmService.getAllFilms();
		assertTrue(filmsFromDB.size() >= 2);
	}
	
	@Test
	public void whenInvalidTitle_thenReturnZeroList() {
		List<Film> fromDBs = filmService.getFilmsByTitle("invalidtitle");
		assertEquals(0, fromDBs.size());
	}
	
	@Test
	public void whenInvalidId_thenReturnNull() {
		Film fromDB = filmService.getFilmById(435345);
		assertEquals(null, fromDB);
	}
	
}
