package dvdrental;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dvdrental.Springdvdrental2Application;
import com.dvdrental.entity.Category;
import com.dvdrental.service.CategoryService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=Springdvdrental2Application.class)
public class CategoryTest {

	@Autowired
	private CategoryService categoryService;

	@Test
	public void testGetCategoryById() {
		Category category = new Category("Thriller");
		categoryService.saveCategory(category);
		
		Category fromDB = categoryService.getCategoryById(category.getCategoryId());
		assertEquals(category.getName(), fromDB.getName());
	}

	@Test
	public void testSaveCategory() {
		Category category = new Category("Thriller");

		assertEquals(0, category.getCategoryId());
		categoryService.saveCategory(category);
		assertNotEquals(0, category.getCategoryId());
		Category fromDB = categoryService.getCategoryById(category.getCategoryId());
		assertEquals(category.getName(), fromDB.getName());
	}
	
	@Test
	public void testUpdateCategory() {
		Category category = new Category("Thriller");
		categoryService.saveCategory(category);
		
		category.setName("hniv");
		categoryService.updateCategory(category);
		
		Category fromDB = categoryService.getCategoryById(category.getCategoryId());
		assertEquals("hniv", fromDB.getName());
	}
	
	@Test
	public void testDeleteCategory() {
		Category category = new Category("Thriller");
		categoryService.saveCategory(category);
		
		assertNotEquals(0, category.getCategoryId());
		categoryService.deleteCategory(category.getCategoryId());
	
		Category fromDB = categoryService.getCategoryById(category.getCategoryId());
		assertEquals(null, fromDB);
	}

	@Test
	public void testGetAllCategory() {
		Category category = new Category("Thriller");
		Category category1 = new Category("Thriller1");
		Category category2 = new Category("Thriller2");
		
		categoryService.saveCategory(category);
		categoryService.saveCategory(category1);
		categoryService.saveCategory(category2);

		List<Category> categoriesFromDB = categoryService.getAllCategories();
		assertTrue(categoriesFromDB.size() >= 3);
	}

	@Test
	public void whenInvalidId_thenReturnNull() {
		Category fromDB = categoryService.getCategoryById(435345);
		assertEquals(null, fromDB);
	}

}
