package com.dvdrental.dao;

import java.util.List;

import com.dvdrental.entity.Address;

public interface AddressDao {
	
	void 			saveAddress(Address address);
	
	List<Address> 	getAllAdress();
	
	Address			getAddressById(int id);
	
	void 			updateAddress(Address address);
	
	void 			deleteAddress(Address address);
	
}
