package com.dvdrental.dao;

import java.util.List;

import com.dvdrental.entity.Customer;

public interface CustomerDao {

	void 			saveCustomer(Customer customer);

	List<Customer> 	getAllCustomers();
	
	List<Customer> 	getAllCustomersByPage(String orderBy, int page, int pageSize);

	Customer 		getCustomerById(int id);
	
	List<Customer> 	getCustomersByName(String name);

	void 			updateCustomer(Customer customer);

	void 			deleteCustomer(int customerId);

}
