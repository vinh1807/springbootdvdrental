package com.dvdrental.dao;

import java.util.List;

import com.dvdrental.entity.Film;

public interface FilmDao {

	void 			saveFilm(Film film);
	
	List<Film> 		getAllFilms();
	
	List<Film> 		getAllFilmsByPage(String orderBy, int page, int pageSize);
	
	Film			getFilmById(int id);
	
	List<Film> 		getFilmsByTitle(String title);
	
	void 			updateFilm(Film film);
	
	void 			deleteFilm(int filmId);
	
}
