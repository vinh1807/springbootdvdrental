package com.dvdrental.dao;

import java.util.List;

import com.dvdrental.entity.Category;

public interface CategoryDao {
	
	void 			saveCategory(Category category);
	
	List<Category> 	getAllCategories();
	
	Category		getCategoryById(int id);
	
	void 			updateCategory(Category category);
	
	void 			deleteCategory(int categoryId);

}
