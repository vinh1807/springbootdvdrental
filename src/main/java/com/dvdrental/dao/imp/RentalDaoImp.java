package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.RentalDao;
import com.dvdrental.entity.Rental;

@Repository
public class RentalDaoImp implements RentalDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveRental(Rental rental) {
		sessionFactory.getCurrentSession().save(rental);
	}

	public List<Rental> getAllRentals() {
		@SuppressWarnings("unchecked")
		TypedQuery<Rental> query = sessionFactory.getCurrentSession().createQuery("from Rental");
		return query.getResultList();
	}
	
	public List<Rental> getAllRentalsByPage(String orderBy, int page, int pageSize) {
		@SuppressWarnings("unchecked")
		TypedQuery<Rental> query = sessionFactory.getCurrentSession().createQuery("from Rental rental "
				+ "order by rental." + orderBy);
		query.setFirstResult((page*pageSize)-pageSize);
		query.setMaxResults(pageSize);
		return query.getResultList();
	}

	public Rental getRentalById(int id) {
		Rental rental = sessionFactory.getCurrentSession().find(Rental.class, id);
		return rental;
	}

	public void updateRental(Rental rental) {
		sessionFactory.getCurrentSession().update(rental);	
	}

	public void deleteRental(int rentalId) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(session.find(Rental.class, rentalId));
	}

}
