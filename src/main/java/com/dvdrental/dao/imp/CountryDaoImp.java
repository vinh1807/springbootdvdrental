package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.CountryDao;
import com.dvdrental.entity.Country;

@Repository
public class CountryDaoImp implements CountryDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveCountry(Country country) {
		sessionFactory.getCurrentSession().save(country);
	}

	public List<Country> getAllCountry() {
		@SuppressWarnings("unchecked")
		TypedQuery<Country> query = sessionFactory.getCurrentSession().createQuery("from Country");
		return query.getResultList();
	}

	public Country getCountryById(int id) {
		Country country = sessionFactory.getCurrentSession().find(Country.class, id);
		return country;
	}

	public void updateCountry(Country country) {
		sessionFactory.getCurrentSession().update(country);	
	}

	public void deleteCountry(Country country) {
		sessionFactory.getCurrentSession().delete(country);
	}

}
