package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.CustomerDao;
import com.dvdrental.entity.Customer;

@Repository
public class CustomerDaoImp implements CustomerDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveCustomer(Customer customer) {
		sessionFactory.getCurrentSession().save(customer);
	}

	public List<Customer> getAllCustomers() {
		@SuppressWarnings("unchecked")
		TypedQuery<Customer> query = sessionFactory.getCurrentSession().createQuery("from Customer");
		return query.getResultList();
	}

	public List<Customer> getAllCustomersByPage(String orderBy, int page, int pageSize) {
		@SuppressWarnings("unchecked")
		TypedQuery<Customer> query = sessionFactory.getCurrentSession().createQuery("from Customer customer "
				+ "order by customer." + orderBy);
		query.setFirstResult((page*pageSize)-pageSize);
		query.setMaxResults(pageSize);
		return query.getResultList();
	}

	public Customer getCustomerById(int id) {
		Customer customer = sessionFactory.getCurrentSession().find(Customer.class, id);
		return customer;
	}

	public void updateCustomer(Customer customer) {
		sessionFactory.getCurrentSession().update(customer);	
	}

	public void deleteCustomer(int customerId) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(session.find(Customer.class, customerId));
	}
	
	public List<Customer> getCustomersByName(String name) {
		@SuppressWarnings("unchecked")
		List<Customer> customers = sessionFactory.getCurrentSession().createQuery("select a "
				+ "from Customer a "
				+ "where lower(a.firstName) "
				+ "like :name").setParameter("name", "%"+name+"%").list();
		return customers;
	}

}
