package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;



import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.AddressDao;
import com.dvdrental.entity.Address;

@Repository
public class AddressDaoImp implements AddressDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveAddress(Address address) {
		sessionFactory.getCurrentSession().save(address);
	}

	public List<Address> getAllAdress() {
		@SuppressWarnings("unchecked")
		TypedQuery<Address> query = sessionFactory.getCurrentSession().createQuery("from Address");
		return query.getResultList();
	}

	public Address getAddressById(int id) {
		Address address = sessionFactory.getCurrentSession().get(Address.class, id);
		return address;
	}

	public void updateAddress(Address address) {
		sessionFactory.getCurrentSession().update(address);	
	}

	public void deleteAddress(Address address) {
		sessionFactory.getCurrentSession().delete(address);
	}

}
