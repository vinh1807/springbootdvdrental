package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.ActorDao;
import com.dvdrental.entity.Actor;

@Repository
public class ActorDaoImp implements ActorDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void saveActor(Actor actor) {
		sessionFactory.getCurrentSession().save(actor);
	}

	public List<Actor> getAllActors() {
		@SuppressWarnings("unchecked")
		TypedQuery<Actor> query = sessionFactory.getCurrentSession().createQuery("from Actor actor order by actor.actorId");
		return query.getResultList();
	}
	
	public List<Actor> getAllActorsByPage(String orderBy, int page, int pageSize) {
		@SuppressWarnings("unchecked")
		TypedQuery<Actor> query = sessionFactory.getCurrentSession().createQuery("from Actor actor "
				+ "order by actor." + orderBy);
		query.setFirstResult((page*pageSize)-pageSize);
		query.setMaxResults(pageSize);
		return query.getResultList();
	}

	public Actor getActorById(int id) {
		Actor actor = sessionFactory.getCurrentSession().find(Actor.class, id);
		return actor;
	}

	public void updateActor(Actor actor) {
		sessionFactory.getCurrentSession().update(actor);	
	}

	public void deleteActor(int actorId) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(session.find(Actor.class, actorId));
	}

	public List<Actor> getActorByName(String name) {
		@SuppressWarnings("unchecked")
		List<Actor> actors = sessionFactory.getCurrentSession().createQuery("select a "
				+ "from Actor a "
				+ "where lower(a.firstName) "
				+ "like :name").setParameter("name", "%"+name+"%").list();
		return actors;
	}

}
