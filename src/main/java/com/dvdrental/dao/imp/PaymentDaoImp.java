package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.PaymentDao;
import com.dvdrental.entity.Payment;

@Repository
public class PaymentDaoImp implements PaymentDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void savePayment(Payment payment) {
		sessionFactory.getCurrentSession().save(payment);
	}

	public List<Payment> getAllPayments() {
		@SuppressWarnings("unchecked")
		TypedQuery<Payment> query = sessionFactory.getCurrentSession().createQuery("from Payment");
		return query.getResultList();
	}

	public Payment getPaymentById(int id) {
		Payment payment = sessionFactory.getCurrentSession().find(Payment.class, id);
		return payment;
	}

	public void updatePayment(Payment payment) {
		sessionFactory.getCurrentSession().update(payment);	
	}

	public void deletePayment(int paymentId) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(session.find(Payment.class, paymentId));
	}

}
