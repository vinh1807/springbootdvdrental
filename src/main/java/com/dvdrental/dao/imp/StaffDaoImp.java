package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.StaffDao;
import com.dvdrental.entity.Staff;

@Repository
public class StaffDaoImp implements StaffDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveStaff(Staff staff) {
		sessionFactory.getCurrentSession().save(staff);
	}

	public List<Staff> getAllStaffs() {
		@SuppressWarnings("unchecked")
		TypedQuery<Staff> query = sessionFactory.getCurrentSession().createQuery("from Staff");
		return query.getResultList();
	}

	public Staff getStaffById(int id) {
		Staff staff = sessionFactory.getCurrentSession().find(Staff.class, id);
		return staff;
	}

	public void updateStaff(Staff staff) {
		sessionFactory.getCurrentSession().update(staff);	
	}

	public void deleteStaff(int staffId) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(session.find(Staff.class, staffId));
	}

}
