package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.InventoryDao;
import com.dvdrental.entity.Inventory;

@Repository
public class InventoryDaoImp implements InventoryDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveInventory(Inventory inventory) {
		sessionFactory.getCurrentSession().save(inventory);
	}

	public List<Inventory> getAllInventories() {
		@SuppressWarnings("unchecked")
		TypedQuery<Inventory> query = sessionFactory.getCurrentSession().createQuery("from Inventory");
		return query.getResultList();
	}

	public Inventory getInventoryById(int id) {
		Inventory inventory = sessionFactory.getCurrentSession().find(Inventory.class, id);
		return inventory;
	}

	public void updateInventory(Inventory inventory) {
		sessionFactory.getCurrentSession().update(inventory);	
	}

	public void deleteInventory(Inventory inventory) {
		sessionFactory.getCurrentSession().delete(inventory);
	}

}
