package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.CategoryDao;
import com.dvdrental.entity.Category;

@Repository
public class CategoryDaoImp implements CategoryDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveCategory(Category category) {
		sessionFactory.getCurrentSession().save(category);
	}

	public List<Category> getAllCategories() {
		@SuppressWarnings("unchecked")
		TypedQuery<Category> query = sessionFactory.getCurrentSession().createQuery("from Category");
		return query.getResultList();
	}

	public Category getCategoryById(int id) {
		Category category = sessionFactory.getCurrentSession().find(Category.class, id);
		return category;
	}

	public void updateCategory(Category category) {
		sessionFactory.getCurrentSession().update(category);	
	}

	public void deleteCategory(int categoryId) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(session.find(Category.class, categoryId));
	}

}
