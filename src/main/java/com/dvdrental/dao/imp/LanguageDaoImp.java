package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.LanguageDao;
import com.dvdrental.entity.Language;

@Repository
public class LanguageDaoImp implements LanguageDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveLanguage(Language language) {
		sessionFactory.getCurrentSession().save(language);
	}

	public List<Language> getAllLanguages() {
		@SuppressWarnings("unchecked")
		TypedQuery<Language> query = sessionFactory.getCurrentSession().createQuery("from Language");
		return query.getResultList();
	}

	@Transactional
	public Language getLanguageById(int id) {
		Language language = sessionFactory.getCurrentSession().find(Language.class, id);
		return language;
	}

	public void updateLanguage(Language language) {
		sessionFactory.getCurrentSession().update(language);	
	}

	public void deleteLanguage(Language language) {
		sessionFactory.getCurrentSession().delete(language);
	}
	
}
