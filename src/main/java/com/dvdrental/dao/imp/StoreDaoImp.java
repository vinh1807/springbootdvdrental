package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.entity.Store;

@Repository
public class StoreDaoImp {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public void saveStore(Store store) {
		sessionFactory.getCurrentSession().save(store);
	}

	@Transactional
	public List<Store> getAllStores() {
		@SuppressWarnings("unchecked")
		TypedQuery<Store> query = sessionFactory.getCurrentSession().createQuery("from Store");
		return query.getResultList();
	}

	@Transactional
	public Store getStoreById(int id) {
		Store store = sessionFactory.getCurrentSession().find(Store.class, id);
		return store;
	}

	@Transactional
	public void updateStore(Store store) {
		sessionFactory.getCurrentSession().update(store);	
	}

	@Transactional
	public void deleteStore(Store store) {
		sessionFactory.getCurrentSession().delete(store);
	}

}
