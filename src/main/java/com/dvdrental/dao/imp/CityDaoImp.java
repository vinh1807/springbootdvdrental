package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.CityDao;
import com.dvdrental.entity.City;

@Repository
public class CityDaoImp implements CityDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveCity(City city) {
		sessionFactory.getCurrentSession().save(city);
	}

	public List<City> getAllCity() {
		@SuppressWarnings("unchecked")
		TypedQuery<City> query = sessionFactory.getCurrentSession().createQuery("from City");
		return query.getResultList();
	}

	public City getCityById(int id) {
		City city = sessionFactory.getCurrentSession().find(City.class, id);
		return city;
	}

	public void updateCity(City city) {
		sessionFactory.getCurrentSession().update(city);	
	}

	public void deleteCity(City city) {
		sessionFactory.getCurrentSession().delete(city);
	}

}
