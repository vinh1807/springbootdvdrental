package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.FilmDao;
import com.dvdrental.entity.Film;

@Repository
public class FilmDaoImp implements FilmDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveFilm(Film film) {
		sessionFactory.getCurrentSession().save(film);
	}

	public List<Film> getAllFilms() {
		@SuppressWarnings("unchecked")
		TypedQuery<Film> query = sessionFactory.getCurrentSession().createQuery("from Film film order by film.filmId");
		return query.getResultList();
	}
	
	public List<Film> getAllFilmsByPage(String orderBy, int page, int pageSize) {
		@SuppressWarnings("unchecked")
		TypedQuery<Film> query = sessionFactory.getCurrentSession().createQuery("from Film film "
				+ "order by film." + orderBy);
		query.setFirstResult((page*pageSize)-pageSize);
		query.setMaxResults(pageSize);
		return query.getResultList();
	}

	public Film getFilmById(int id) {
		Film film = sessionFactory.getCurrentSession().find(Film.class, id);
		return film;
	}

	public void updateFilm(Film film) {
		sessionFactory.getCurrentSession().update(film);
	}

	public void deleteFilm(int filmId) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(session.find(Film.class, filmId));
	}
	
	public List<Film> getFilmsByTitle(String title) {
		@SuppressWarnings("unchecked")
		List<Film> films = sessionFactory.getCurrentSession().createQuery("select f "
				+ "from Film f "
				+ "where lower(f.title) "
				+ "like :title").setParameter("title", "%"+title+"%").list();
		return films;
	}

}
