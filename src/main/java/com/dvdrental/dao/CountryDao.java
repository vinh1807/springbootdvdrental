package com.dvdrental.dao;

import java.util.List;

import com.dvdrental.entity.Country;

public interface CountryDao {

	void 			saveCountry(Country country);
	
	List<Country> 	getAllCountry();
	
	Country			getCountryById(int id);
	
	void 			updateCountry(Country country);
	
	void 			deleteCountry(Country country);
	
}
