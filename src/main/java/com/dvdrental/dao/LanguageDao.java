package com.dvdrental.dao;

import java.util.List;

import com.dvdrental.entity.Language;

public interface LanguageDao {

	void 			saveLanguage(Language language);
	
	List<Language> 	getAllLanguages();
	
	Language		getLanguageById(int id);
	
	void 			updateLanguage(Language language);
	
	void 			deleteLanguage(Language language);
	
}
