package com.dvdrental.dao;

import java.util.List;

import com.dvdrental.entity.City;

public interface CityDao {

	void 			saveCity(City city);
	
	List<City> 		getAllCity();
	
	City			getCityById(int id);
	
	void 			updateCity(City city);
	
	void 			deleteCity(City city);
	
}
