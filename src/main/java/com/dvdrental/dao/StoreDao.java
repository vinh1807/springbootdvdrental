package com.dvdrental.dao;

import java.util.List;

import com.dvdrental.entity.Store;

public interface StoreDao {

	void 			saveStore(Store store);
	
	List<Store> 	getAllStores();
	
	Store			getStoreById(int id);
	
	void 			updateStore(Store store);
	
	void 			deleteStore(Store store);
	
}
