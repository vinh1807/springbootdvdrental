package com.dvdrental.dao;

import java.util.List;

import com.dvdrental.entity.Inventory;

public interface InventoryDao {

	void 				saveInventory(Inventory inventory);
	
	List<Inventory> 	getAllInventories();
	
	Inventory			getInventoryById(int id);
	
	void 				updateInventory(Inventory inventory);
	
	void 				deleteInventory(Inventory inventory);
	
}
