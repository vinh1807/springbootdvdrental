package com.dvdrental.service;

import java.util.List;

import com.dvdrental.entity.Category;

public interface CategoryService {

	void 			saveCategory(Category category);
	
	List<Category> 	getAllCategories();
	
	Category		getCategoryById(int id);
	
	void 			updateCategory(Category category);
	
	void 			deleteCategory(int category);
	
}
