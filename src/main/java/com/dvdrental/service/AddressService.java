package com.dvdrental.service;

import java.util.List;

import com.dvdrental.entity.Address;

public interface AddressService {

	void 			saveAddress(Address address);
	
	List<Address> 	getAllAddresses();
	
	Address			getAddressById(int id);
	
	void 			updateAddress(Address address);
	
	void 			deleteAddress(Address address);
	
}
