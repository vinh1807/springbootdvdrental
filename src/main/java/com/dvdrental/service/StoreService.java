package com.dvdrental.service;

import java.util.List;

import com.dvdrental.entity.Store;

public interface StoreService {

	void 			saveStore(Store store);
	
	List<Store> 	getAllStores();
	
	Store			getStoreById(int id);
	
	void 			updateStore(Store store);
	
	void 			deleteStore(Store store);
	
}
