package com.dvdrental.service;

import java.util.List;

import com.dvdrental.entity.Rental;

public interface RentalService {
	
	void 			saveRental(Rental rental);
	
	List<Rental> 	getAllRentals();

	List<Rental> 	getAllRentalsByPage(String orderBy, int page, int pageSize);
	
	Rental			getRentalById(int id);
	
	void 			updateRental(Rental rental);
	
	void 			deleteRental(int rentalId);
	
}
