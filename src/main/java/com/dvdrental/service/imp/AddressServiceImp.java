package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.AddressDao;
import com.dvdrental.entity.Address;
import com.dvdrental.service.AddressService;

@Service
public class AddressServiceImp implements AddressService {

	@Autowired
	AddressDao addressDao;
	
	@Transactional
	public void saveAddress(Address address) {
		addressDao.saveAddress(address);
	}

	@Transactional
	public List<Address> getAllAddresses() {
		return addressDao.getAllAdress();
	}

	@Transactional
	public Address getAddressById(int id) {
		return addressDao.getAddressById(id);
	}

	@Transactional
	public void updateAddress(Address address) {
		addressDao.updateAddress(address);
	}

	@Transactional
	public void deleteAddress(Address address) {
		addressDao.deleteAddress(address);
	}

}
