package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.CustomerDao;
import com.dvdrental.entity.Customer;
import com.dvdrental.service.CustomerService;

@Service
public class CustomerServiceImp implements CustomerService {
	
	@Autowired
	CustomerDao customerDao;
	
	@Transactional
	public void saveCustomer(Customer customer) {
		customerDao.saveCustomer(customer);
	}

	@Transactional
	public List<Customer> getAllCustomers() {
		return customerDao.getAllCustomers();
	}

	@Transactional
	public List<Customer> getAllCustomersByPage(String orderBy, int page, int pageSize) {
		return customerDao.getAllCustomersByPage(orderBy, page, pageSize);
	}


	@Transactional
	public Customer getCustomerById(int id) {
		return customerDao.getCustomerById(id);
	}
	
	@Transactional
	public void updateCustomer(Customer customer) {
		customerDao.updateCustomer(customer);
	}

	@Transactional
	public void deleteCustomer(int customerId) {
		customerDao.deleteCustomer(customerId);
	}

	@Transactional
	public List<Customer> getCustomersByName(String name) {
		return customerDao.getCustomersByName(name);
	}

}
