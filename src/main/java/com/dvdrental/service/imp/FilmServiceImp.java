package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.FilmDao;
import com.dvdrental.entity.Film;
import com.dvdrental.service.FilmService;

@Service
public class FilmServiceImp implements FilmService {
	
	@Autowired
	FilmDao filmDao;
	
	@Transactional
	public void saveFilm(Film film) {
		filmDao.saveFilm(film);
	}

	@Transactional
	public List<Film> getAllFilms() {
		return filmDao.getAllFilms();
	}
	
	@Transactional
	public List<Film> getAllFilmsByPage(String orderBy, int page, int pageSize) {
		return filmDao.getAllFilmsByPage(orderBy, page, pageSize);
	}

	@Transactional
	public Film getFilmById(int id) {
		return filmDao.getFilmById(id);
	}

	@Transactional
	public void updateFilm(Film film) {
		filmDao.updateFilm(film);
	}

	@Transactional
	public void deleteFilm(int filmId) {
		filmDao.deleteFilm(filmId);
	}

	@Transactional
	public List<Film> getFilmsByTitle(String title) {
		return filmDao.getFilmsByTitle(title);
	}

}
