package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.RentalDao;
import com.dvdrental.entity.Rental;
import com.dvdrental.service.RentalService;

@Service
public class RentalServiceImp implements RentalService {

	@Autowired
	RentalDao rentalDao;
	
	@Transactional
	public void saveRental(Rental rental) {
		rentalDao.saveRental(rental);
	}

	@Transactional
	public List<Rental> getAllRentals() {
		return rentalDao.getAllRentals();
	}
	
	@Transactional
	public List<Rental> getAllRentalsByPage(String orderBy, int page, int pageSize) {
		return rentalDao.getAllRentalsByPage(orderBy, page, pageSize);
	}

	@Transactional
	public Rental getRentalById(int id) {
		return rentalDao.getRentalById(id);
	}

	@Transactional
	public void updateRental(Rental rental) {
		rentalDao.updateRental(rental);
	}

	@Transactional
	public void deleteRental(int rentalId) {
		rentalDao.deleteRental(rentalId);
	}

}
