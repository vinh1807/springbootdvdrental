package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.CategoryDao;
import com.dvdrental.entity.Category;
import com.dvdrental.service.CategoryService;

@Service
public class CategoryServiceImp implements CategoryService {

	@Autowired
	CategoryDao categoryDao;
	
	@Transactional
	public void saveCategory(Category category) {
		categoryDao.saveCategory(category);
	}

	@Transactional
	public List<Category> getAllCategories() {
		return categoryDao.getAllCategories();
	}

	@Transactional
	public Category getCategoryById(int id) {
		return categoryDao.getCategoryById(id);
	}

	@Transactional
	public void updateCategory(Category category) {
		categoryDao.updateCategory(category);
	}

	@Transactional
	public void deleteCategory(int categoryId) {
		categoryDao.deleteCategory(categoryId);
	}

}
