package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.CityDao;
import com.dvdrental.entity.City;
import com.dvdrental.service.CityService;

@Service
public class CityServiceImp implements CityService {
	
	@Autowired
	CityDao cityDao;
	
	@Transactional
	public void saveCity(City city) {
		cityDao.saveCity(city);
	}

	@Transactional
	public List<City> getAllCity() {
		return cityDao.getAllCity();
	}

	@Transactional
	public City getCityById(int id) {
		return cityDao.getCityById(id);
	}

	@Transactional
	public void updateCity(City city) {
		cityDao.updateCity(city);
	}

	@Transactional
	public void deleteCity(City city) {
		cityDao.deleteCity(city);
	}

}
