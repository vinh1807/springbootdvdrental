package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.InventoryDao;
import com.dvdrental.entity.Inventory;
import com.dvdrental.service.InventoryService;

@Service
public class InventoryServiceImp implements InventoryService {

	@Autowired
	InventoryDao inventoryDao;
	
	@Transactional
	public void saveInventory(Inventory inventory) {
		inventoryDao.saveInventory(inventory);
	}

	@Transactional
	public List<Inventory> getAllInventories() {
		return inventoryDao.getAllInventories();
	}

	@Transactional
	public Inventory getInventoryById(int id) {
		return inventoryDao.getInventoryById(id);
	}

	@Transactional
	public void updateInventory(Inventory inventory) {
		inventoryDao.updateInventory(inventory);
	}

	@Transactional
	public void deleteInventory(Inventory inventory) {
		inventoryDao.deleteInventory(inventory);
	}

}
