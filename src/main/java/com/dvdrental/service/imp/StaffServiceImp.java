package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.StaffDao;
import com.dvdrental.entity.Staff;
import com.dvdrental.service.StaffService;

@Service
public class StaffServiceImp implements StaffService {

	@Autowired
	StaffDao staffDao;
	
	@Transactional
	public void saveStaff(Staff staff) {
		staffDao.saveStaff(staff);
	}

	@Transactional
	public List<Staff> getAllStaffs() {
		return staffDao.getAllStaffs();
	}

	@Transactional
	public Staff getStaffById(int id) {
		return staffDao.getStaffById(id);
	}

	@Transactional
	public void updateStaff(Staff staff) {
		staffDao.updateStaff(staff);
	}

	@Transactional
	public void deleteStaff(int staffId) {
		staffDao.deleteStaff(staffId);
	}

}
