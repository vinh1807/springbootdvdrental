package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.LanguageDao;
import com.dvdrental.entity.Language;
import com.dvdrental.service.LanguageService;

@Service
public class LanguageServiceImp implements LanguageService {
	
	@Autowired
	LanguageDao languageDao;
	
	@Transactional
	public void saveLanguage(Language language) {
		languageDao.saveLanguage(language);
	}

	@Transactional
	public List<Language> getAllLanguages() {
		return languageDao.getAllLanguages();
	}

	@Transactional
	public Language getLanguageById(int id) {
		return languageDao.getLanguageById(id);
	}

	@Transactional
	public void updateLanguage(Language language) {
		languageDao.updateLanguage(language);
	}

	@Transactional
	public void deleteLanguage(Language language) {
		languageDao.deleteLanguage(language);
	}

}
