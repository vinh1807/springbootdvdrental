package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.PaymentDao;
import com.dvdrental.entity.Payment;
import com.dvdrental.service.PaymentService;

@Service
public class PaymentServiceImp implements PaymentService {

	@Autowired
	PaymentDao paymentDao;
	
	@Transactional
	public void savePayment(Payment payment) {
		paymentDao.savePayment(payment);
	}

	@Transactional
	public List<Payment> getAllPayments() {
		return paymentDao.getAllPayments();
	}

	@Transactional
	public Payment getPaymentById(int id) {
		return paymentDao.getPaymentById(id);
	}

	@Transactional
	public void updatePayment(Payment payment) {
		paymentDao.updatePayment(payment);
	}

	@Transactional
	public void deletePayment(int paymentId) {
		paymentDao.deletePayment(paymentId);
	}

}
