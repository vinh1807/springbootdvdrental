package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.StoreDao;
import com.dvdrental.entity.Store;
import com.dvdrental.service.StoreService;

public class StoreServiceImp implements StoreService {

	@Autowired
	StoreDao storeDao;
	
	@Transactional
	public void saveStore(Store store) {
		storeDao.saveStore(store);
	}

	@Transactional
	public List<Store> getAllStores() {
		return storeDao.getAllStores();
	}

	@Transactional
	public Store getStoreById(int id) {
		return storeDao.getStoreById(id);
	}

	@Transactional
	public void updateStore(Store store) {
		storeDao.updateStore(store);
	}

	@Transactional
	public void deleteStore(Store store) {
		storeDao.deleteStore(store);
	}

}
