package com.dvdrental.service;

import java.util.List;

import com.dvdrental.entity.Language;

public interface LanguageService {
	
	void 			saveLanguage(Language film);
	
	List<Language> 	getAllLanguages();
	
	Language		getLanguageById(int id);
	
	void 			updateLanguage(Language film);
	
	void 			deleteLanguage(Language film);

}
