package com.dvdrental.service;

import java.util.List;

import com.dvdrental.entity.Staff;

public interface StaffService {

	void 			saveStaff(Staff staff);
	
	List<Staff> 	getAllStaffs();
	
	Staff			getStaffById(int id);
	
	void 			updateStaff(Staff staff);
	
	void 			deleteStaff(int staffId);
	
}
