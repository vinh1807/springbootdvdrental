package com.dvdrental.service;

import java.util.List;

import com.dvdrental.entity.Payment;

public interface PaymentService {

	void 			savePayment(Payment payment);
	
	List<Payment> 	getAllPayments();
	
	Payment			getPaymentById(int id);
	
	void 			updatePayment(Payment payment);
	
	void 			deletePayment(int paymentId);
	
}
