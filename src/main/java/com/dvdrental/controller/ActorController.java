package com.dvdrental.controller;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dvdrental.entity.Actor;
import com.dvdrental.entity.Film;
import com.dvdrental.service.ActorService;

@RestController
@RequestMapping("actor")
public class ActorController {

	@Autowired
	private ActorService actorService;

	@PostMapping("/")
	public ResponseEntity<?> saveActor(@RequestBody Actor actor) {
		actorService.saveActor(actor);
		return ResponseEntity.ok().body(actor);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Actor> getActorById(@PathVariable("id") int id) {
		Actor actor = actorService.getActorById(id);
		return ResponseEntity.ok().body(actor);
	}

	@GetMapping("/all")
	public ResponseEntity<List<Actor>> getAllActors() {
		List<Actor> actors = actorService.getAllActors();
		for (Actor actor : actors) {
			actor.setFilms(new HashSet<Film>());
		}
		return ResponseEntity.ok().body(actors);
	}

	@GetMapping("/")
	public ResponseEntity<List<Actor>> getAllActorsByPage(@RequestParam(value = "orderBy", defaultValue = "actorId") String orderBy,
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
		List<Actor> actors = actorService.getAllActorsByPage(orderBy, page, pageSize);
		return ResponseEntity.ok().body(actors);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteActor(@PathVariable("id") int id) {
		actorService.deleteActor(id);
		return ResponseEntity.ok().body(id);
	}

	@PutMapping("/")
	public ResponseEntity<?> update(@RequestBody Actor actor) {
		actorService.updateActor(actor);
		return ResponseEntity.ok().body(actor);
	}

}
