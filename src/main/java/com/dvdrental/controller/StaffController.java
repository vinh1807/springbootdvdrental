package com.dvdrental.controller;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dvdrental.entity.Staff;
import com.dvdrental.entity.Payment;
import com.dvdrental.entity.Rental;
import com.dvdrental.service.StaffService;

@RestController
@RequestMapping("staff")
public class StaffController {

	@Autowired
	private StaffService staffService;

	@PostMapping("/")
	public ResponseEntity<?> saveStaff(@RequestBody Staff staff) {
		staffService.saveStaff(staff);
		return ResponseEntity.ok().body(staff);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Staff> getStaffById(@PathVariable("id") int id) {
		Staff staff = staffService.getStaffById(id);
		return ResponseEntity.ok().body(staff);
	}

	@GetMapping("/all")
	public ResponseEntity<List<Staff>> getAllStaffs() {
		List<Staff> staffs = staffService.getAllStaffs();
		for (Staff staff : staffs) {
			staff.setRentals(new HashSet<Rental>());
			staff.setPayments(new HashSet<Payment>());
		}
		return ResponseEntity.ok().body(staffs);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteStaff(@PathVariable("id") int id) {
		staffService.deleteStaff(id);
		return ResponseEntity.ok().body(id);
	}

	@PutMapping("/")
	public ResponseEntity<?> update(@RequestBody Staff staff) {
		staffService.updateStaff(staff);
		return ResponseEntity.ok().body(staff);
	}
	
}
