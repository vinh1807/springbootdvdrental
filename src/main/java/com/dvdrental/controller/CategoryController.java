package com.dvdrental.controller;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dvdrental.entity.Category;
import com.dvdrental.entity.Film;
import com.dvdrental.service.CategoryService;

@RestController
@RequestMapping("category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@PostMapping("/")
	public ResponseEntity<?> saveCategory(@RequestBody Category category) {
		categoryService.saveCategory(category);
		return ResponseEntity.ok().body(category);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Category> getCategoryById(@PathVariable("id") int id) {
		Category category = categoryService.getCategoryById(id);
		return ResponseEntity.ok().body(category);
	}

	@GetMapping("/all")
	public ResponseEntity<List<Category>> getAllCategories() {
		List<Category> categories = categoryService.getAllCategories();
		for (Category category : categories) {
			category.setFilms(new HashSet<Film>());
		}
		return ResponseEntity.ok().body(categories);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteCategory(@PathVariable("id") int id) {
		categoryService.deleteCategory(id);
		return ResponseEntity.ok().body(id);
	}

	@PutMapping("/")
	public ResponseEntity<?> update(@RequestBody Category category) {
		categoryService.updateCategory(category);
		return ResponseEntity.ok().body(category);
	}

}
