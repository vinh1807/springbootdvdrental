package com.dvdrental.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dvdrental.entity.Rental;
import com.dvdrental.service.RentalService;

@RestController
@RequestMapping("rental")
public class RentalController {

	@Autowired
	private RentalService rentalService;

	@PostMapping("/")
	public ResponseEntity<?> saveRental(@RequestBody Rental rental) {
		rentalService.saveRental(rental);
		return ResponseEntity.ok().body(rental);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Rental> getRentalById(@PathVariable("id") int id) {
		Rental rental = rentalService.getRentalById(id);
		return ResponseEntity.ok().body(rental);
	}

	@GetMapping("/all")
	public ResponseEntity<List<Rental>> getAllRentals() {
		List<Rental> rentals = rentalService.getAllRentals();
		return ResponseEntity.ok().body(rentals);
	}

	@GetMapping("/")
	public ResponseEntity<List<Rental>> getAllRentalsByPage(@RequestParam(value = "orderBy", defaultValue = "rentalId") String orderBy,
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
		List<Rental> rentals = rentalService.getAllRentalsByPage(orderBy, page, pageSize);
		return ResponseEntity.ok().body(rentals);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteRental(@PathVariable("id") int id) {
		rentalService.deleteRental(id);
		return ResponseEntity.ok().body(id);
	}

	@PutMapping("/")
	public ResponseEntity<?> update(@RequestBody Rental rental) {
		rentalService.updateRental(rental);
		return ResponseEntity.ok().body(rental);
	}
	
}
