package com.dvdrental.controller;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dvdrental.entity.Customer;
import com.dvdrental.entity.Payment;
import com.dvdrental.entity.Rental;
import com.dvdrental.service.CustomerService;

@RestController
@RequestMapping("customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@PostMapping("/")
	public ResponseEntity<?> saveCustomer(@RequestBody Customer customer) {
		customerService.saveCustomer(customer);
		return ResponseEntity.ok().body(customer);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable("id") int id) {
		Customer customer = customerService.getCustomerById(id);
		return ResponseEntity.ok().body(customer);
	}

	@GetMapping("/all")
	public ResponseEntity<List<Customer>> getAllCustomers() {
		List<Customer> customers = customerService.getAllCustomers();
		for (Customer customer : customers) {
			customer.setRentals(new HashSet<Rental>());
			customer.setPayments(new HashSet<Payment>());
		}
		return ResponseEntity.ok().body(customers);
	}

	@GetMapping("/")
	public ResponseEntity<List<Customer>> getAllCustomersByPage(@RequestParam(value = "orderBy", defaultValue = "customerId") String orderBy,
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
		List<Customer> customers = customerService.getAllCustomersByPage(orderBy, page, pageSize);
		return ResponseEntity.ok().body(customers);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteCustomer(@PathVariable("id") int id) {
		customerService.deleteCustomer(id);
		return ResponseEntity.ok().body(id);
	}

	@PutMapping("/")
	public ResponseEntity<?> update(@RequestBody Customer customer) {
		customerService.updateCustomer(customer);
		return ResponseEntity.ok().body(customer);
	}
	
}
